%Simulation of Kuramoto for 2 oscillators

function[t] = kuramoto3_b(b,p)

N=2;

% Total number of timesteps
T= 100;
K= 2.2;
%Time step size. As tau -> 0 approximation of continumm model improves
tau=.0001;
% Initial conditions with normal distribution for w
sigN=1; 
% w = random('Normal',0,sigN,1,N);% Random on initial conditionskj 

if(p==0)
    
w = [1.4,1]; 

elseif(p==1)
    
    w = [0,1];
end    

% The bifurcation loop
x=1; 

% This generates 260 different levels, from 0.001 to 0.26.
%for j = 520:1:780  
    %w(2)= j/1000;
    
    %prompt = 'Enter the 8 bit digital number as an array';
    
    %digital_data = input(prompt);
    
    w(2) = b;
    
    for i=1:N
      theta(i)= w(i);
    end
    

    % Attempting to calculate r (Phase coherence between 0 and 1) 
    % and phi (Average angle) for the first steps of time
    rx=0;
    ry=0;
    phi(1)=0;
    for i=1:N
        phi(1) = phi(1) + (1/N)*theta(i); % Calculation mean angle phi
        rx=rx+(1/N)*cos(theta(i)); % Sum of mean x-part of theta
        ry=ry+(1/N)*sin(theta(i)); % Sum of mean y-part of theta
    end
    y(1) = sqrt(rx*rx + ry*ry);
    y(2) = y(1);
    %fprintf('r(1) is : %f\n',y(1));
    phi(2)=phi(1);

    % The main loop
    t=2;
%    for t=2:T
     while (y(t) < 0.97) 
        % Initial conditions each timestep in the loop
       rx=0;
         ry=0;
        phi(t+1)= 0;
        % The loop of individuals
        for i=1:N
            % Main equation
            theta(i) = theta(i) + tau*(w(i) + K*y(t)*sin(phi(t)-theta(i)));
            rx=rx+(1/N)*cos(theta(i)); % Sum of mean x-part of theta
            ry=ry+(1/N)*sin(theta(i)); % Sum of mean y-part of theta
            % Calculating mean angle phi for next step of time
            phi(t+1) = phi(t+1) + (1/N)*theta(i);
        end 
        
        t = t+1;
        y(t) = sqrt(rx*rx + ry*ry); % Calculating total mean radius
%         if(y(t)>=0.9700)
%             display(y(t));
%         end
    end
    
    
    
   % fprintf('Time Steps required : %d\n',t);

%      Time(x)= t;
%      Numbers(x)= w(2);
%      x=x+1;

% end

         
%  Numbers
%  Time
%  figure(1)
%  hold off
%  plot(Numbers,Time,'k')%b
%  hlx=xlabel(' Numbers ');
%  hly=ylabel('Time');
% axis([0.1 0.9 1 200])