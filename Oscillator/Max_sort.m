%Simulation of Kuramoto for 2 oscillators

%prompt = 'Enter the number of numbers to be compared';

%input(prompt);
function[Index] = Max_sort(a,n)

no = n; %randi([10,20],1,1);

z = int32(no);

%display(z);

num = randi([4,10],1,1);

d = int32(num);

%display(d);

D = int32(d - 1);

for l=1:z
    

    InNo   =  a(l); %randi([0,2^32],1,1);
    %display(InNo);
   
    Array = de2bi(InNo,32,2,'left-msb');
    %display(Array);
    s(l,1:32) = Array;         %s stores all the numbers as 32 bit sized arrays, in binary.
    %display(s(l,1:32));
end

numbers = int32(d);

e = idivide(z,numbers);
%display(e);

o = z-1;
h = rem(o,D);
q = D - h;

if(z> D*e+ d)
    
    Q = 1;
    
else 
    
    Q = 0;
    
end    


if(h~=0)
for l=no+1:no+q
    
    s(l,1:32) = randi([0,0],1,32);
    %display(s(l,1:32));
    
end  

end

M = e + 1*Q;



for u = 0: M 
    

for i = 1:numbers

  digital_data = s(D*u+i,1:32);
 

for j =1:4
    t = digital_data(8*(j-1)+1:8*(j-1)+ 8);
    a(i,j) = DAC(t);
    %display(a(i,j));    
end

end

w = [0,1];

TP = zeros(1,numbers,'uint64');  % Time period 

t = int64(0);
 

for i = 1:numbers
    
    for j = 1:4
        
        w(2) = a(i,j);
        t    = kuramoto3_b(w(2),1);
        
        if(j==1)
            t = 50000000*t;
        elseif(j==2)
            t = 150000*t;
        elseif(j==3)
            t = 450*t;
        elseif(j==4)
            t = 1*t;
        end
        
        TP(i) = TP(i) + t;
        
    end  
    
end




max = 1;

for j = 2:(length(TP))
    
    if(TP(j)>TP(max))
        
        max = j;
        
    else    
        
    end    
    
end

%display(max);
%display('Is the largest in the set');
s(D*(u+1)+1,1:32) = s(D*u+max,1:32);
%display(s(D*u+max,1:32));

end

%display(s(D*(M+1)+1,1:32));

flag = 1;
Index = 0;   % The index of the largest number
j = 1;

while((j<=z)&&(flag==1))
    
    if(s(j,1:32)==s(D*(M+1)+1,1:32))
        
        Index = j;
        flag = 0;
        
    end
    
    %display(s(j,1:32));
    j = j+1;
end    

 %display('RT');
 %display(Index);
MaxN = bi2de(s(D*(M+1)+1,1:32),2,'left-msb');  % MaxN is the largest no. in dec format.
% display(FN);
% display('is the largest');

    
    