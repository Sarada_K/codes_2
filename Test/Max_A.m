%Simulation of Kuramoto for 2 oscillators



no = randi([10,20],1,1);

z = int32(no);

display(z);

num = randi([4,10],1,1);

d = int32(num);

display(d);

D = int32(d - 1);

for l=1:z
    

    IntN0   =  randi([0,2^32],1,1);
    display(IntN0);
   
    Array = de2bi(IntN0,32,2,'left-msb');
    %display(Array);
    s(l,1:32) = Array; %randi([0,1],1,32);   %s is the array used to store integers as 32 bit sized binary arrays.
    %display(s(l,1:32));
end

numbers = int32(d);

e = idivide(z,numbers);
%display(e);

o = z-1;
h = rem(o,D);
q = D - h;

if(z> D*e+ d)
    
    Q = 1;
    
else 
    
    Q = 0;
    
end    


if(h~=0)
for l=no+1:no+q
    
    s(l,1:32) = randi([0,0],1,32);
    %display(s(l,1:32));
    
end  

end

M = e + 1*Q;

for u = 0: M 
    

for i = 1:numbers

  digital_data = s(D*u+i,1:32);
  % display(digital_data);
 
for j =1:4
    t = digital_data(8*(j-1)+1:8*(j-1)+ 8);
    a(i,j) = DAC(t);
    %display(a(i,j));    
end

end

%display(a);

w = [0,1];

    
TP = zeros(1,numbers,'uint64');

t = int64(0);
 
%display(TP);
for i = 1:numbers
  TP(i) =0;  
    for j = 1:4
        
        w(2) = a(i,j);
        t    = kuramoto3_b(w(2),1);
        
        if(j==1)
            t = 50000000*t;
        elseif(j==2)
            t = 150000*t;
        elseif(j==3)
            t = 450*t;
        elseif(j==4)
            t = t;
        end
        
        %display(t);
        TP(i) = TP(i) + t;
       
        
    end  
    
    %display(TP(i));
            
end


%display(TP);

min = 1;

for j = 2:(length(TP))
    
    if(TP(j)>TP(min))
        
        min = j;
        
    else    
        
    end    
    
end

%display(min);
%display('Is the largest in the set');
s(D*(u+1)+1,1:32) = s(D*u+min,1:32);
%display(s(D*u+max,1:32));

end

display(s(D*(M+1)+1,1:32));
FinalN = bi2de(s(D*(M+1)+1,1:32),2,'left-msb');
display(FinalN);
display('is the largest');

    
    